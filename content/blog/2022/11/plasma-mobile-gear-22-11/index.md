---
title: "Plasma Mobile Gear ⚙ 22.11 is Out"
subtitle: "Updates in Plasma Mobile for September to November 2022"
SPDX-License-Identifier: CC-BY-4.0
date: 2022-11-30
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2022 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2022 Alexey Andreyev <aa13q@ya.ru>
- SPDX-FileCopyrightText: 2022 Bart De Vries <bart@mogwai.be>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js

tokodon:
- name: New timeline view
  url: tokodon-timeline.png
- name: New settings
  url: tokodon-settings.png
dialer:
- name: Dialer answer buttons
  url: dialer-answer-buttons.png
- name: Dialer asymmetric answer swipe
  url: dialer-asymmetric-answer-swipe.png
- name: Dialer symmetric answer swipe
  url: dialer-symmetric-answer-swipe.png
- name: Dialer answer control settings
  url: dialer-answer-control-settings.png
spacebar:
- name: Tapbacks
  url: spacebar-1.png
- name: Sending tapbacks
  url: spacebar-2.png
audiotube:
- name: AudioTube showing an artist's page
  url: audiotube-artist-page.png
- name: AudioTube filtering through known songs
  url: audiotube-search.png
kweather:
- name: Weather view
  url: kweather-1.png
- name: Locations list
  url: kweather-2.png
- name: Add location page
  url: kweather-3.png
krecorder:
- name: Recording list
  url: krecorder-1.png
- name: Recording page
  url: krecorder-2.png
- name: Player page
  url: krecorder-3.png
- name: Settings page
  url: krecorder-4.png
- name: Desktop
  url: krecorder-5.png
- name: Desktop settings
  url: krecorder-6.png
neochat:
- name: Channels list
  url: neochat-1.png
- name: General settings
  url: neochat-2.png
- name: Notification settings
  url: neochat-3.png
- name: Accounts
  url: neochat-4.png
  
---

The Plasma Mobile team is happy to announce the result of all the project's development work carried out in September and November of 2022. 

## Plasma Mobile Gear

We have decided to migrate the releases of Plasma Mobile applications to KDE Gear, starting with KDE Gear 23.04. This means that Plasma Mobile Gear will be discontinued in the future, and Plasma Mobile applications will follow the release schedule of most other KDE applications, simplifying packaging. To prepare for this, an ongoing effort was made to ensure all applications have proper Bugzilla categories created.

## Akademy

Akademy 2022 was held in Barcelona, and Devin and Bhushan presented some of the work in the project in the following talk:

<div class="ratio ratio-16x9">
<iframe src="https://www.youtube-nocookie.com/embed/wSlMtf-YGw4?start=19257" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Several Plasma Mobile BoF (birds-of-feather) meetings were also held. More details about them can be read over at [Devin's blog](https://espi.dev/posts/2022/10/akademy-2022/).

## Shell

Plasma 5.27 will be released on February 9th, 2023. This will be the last Plasma 5 release, with work after that being focused on Plasma 6!

### Action Drawer

Devin added a feature that lets you tap the media player so that the audio source app window opens. He also fixed the quicksettings so that it now always opens the mobile settings application. Several issues with the mobile data quicksetting not accounting for certain scenarios were also fixed; and he also worked on fixing the marquee label in the WiFi quicksetting, stopping them from overflowing when multiple network devices are attached.

### Navigation Panel

Devin fixed the close button not being usable while an application is launching.

### Halcyon (Homescreen)

Devin fixed some performance issues when scrolling in the grid app list. This should improve performance a lot on slower devices.

Yari fixed support for the Meta key, and it should now properly bring up the homescreen when pressed.

### Lockscreen

Devin did some performance refactoring, and also set the clock text to **bold** to improve contrast.

### KScreen

Aleix fixed wakeups while the screen is off and the device is rotated. Rotations are now only tracked when the display is on.

### KWin

Xaver added support for device panel orientations. This means that devices like the OnePlus 5 (which has an upside-down mounted display) will now have the orientation correct by default, and not inverted for touch input.

### Other

The bug that led to shell configurations sometimes being wiped at start has been fixed in the upcoming Plasma 5.26.4 release.

Seshan worked on an updated design for the power menu, and it now includes a logout button.

![New Power Menu design](new-power-menu.png)

## Weather

Devin spent time addressing feedback through the KDEReview process, in preparation for moving the application to the KDE Gear release cycle. These changes include:

* The settings dialog being switched to use a window in desktop mode
* The scrollbars being added to views
* Re-implementing the location list reordering to be much nicer to use
* Many bugfixes

{{< screenshots name="kweather" >}}

## Recorder

Devin also spent time on the Recorder app, addressing feedback through the KDEReview process in preparation for moving the application to the KDE Gear release cycle. These changes include:

* The Recorder page now uses a fullscreen layout
* The recording player layout has been reworked to be easier to use
* The settings dialog is a window in desktop mode
* Recordings now start immediately when the record button is pressed
* You can now export recordings to a different location
* A bug that added suffixes to recorded file names for no reason was corrected
* Many bugfixes and UX improvements

{{< screenshots name="krecorder" >}}

## Clock

Devin fixed an issue where looping timers could have multiple ongoing notifications and the user was not able to dismiss them.

## Terminal

Devin did some bug fixing work on the terminal application. He fixed command deletion not saving in certain cases, and also fixed the bug which made the whole window to close when Ctrl-D was pressed.

## Dialer

According to the feedback obtained after the previous incoming call screen updates, Alexey introduced support for changing the answer controls. He provided buttons, and a selection of asymmetric and symmetric answer swipes. He also implemented call duration and caller id support for the incoming call screen with updates both for the daemon and GUI logic.

Marco, along with Alexey, fixed an issue when there was no ringtone when the phone was in lock screen mode without an additional notification. Marco also helped Alexey improve KWin's logic when parsing application privileges, like the lock screen overlay.

Volker introduced initial support for the Qt6 CI builds.

Devin ported Dialer settings to the new form components.

{{< screenshots name="dialer" >}}

<div class="embed-responsive">
  <video src="dialer-answer-controls.mp4" controls autoplay=false loop muted></video>
</div>

## Spacebar

Michael added attachment previews to notifications. He also made it so that image attachment previews are shown in the chat's list. Another thing he implemented is support for tapback reactions. There is now a confirmation dialog before deleting a chat conversation to prevent accidental deletion of a conversation. Michael also made it so that MMS messages can be downloaded even when wifi is also connected.

{{< screenshots name="spacebar" >}}

## Discover

Aleix worked on a more helpful homepage that better displays featured applications.

## Tokodon

Carl ported Tokodon's settings to the new form components. He also updated the timeline by automatically cropping and rounding the images, improving the typography and fixing some sizing issues.

Volker fixed multiple bugs in the timeline and reduced the transfer volume on a "warm" start-up by 80%. For the technical details, you might want to read his blog post: [Secure and efficient QNetworkAccessManager use](https://www.volkerkrause.eu/2022/11/19/qt-qnetworkaccessmanager-best-practices.html)

{{< screenshots name="tokodon" >}}

## NeoChat

Tobias has made a lot of progress on end-to-end encryption. You can read more about it in his blog post: [NeoChat, encryption, and thanks for all the olms](https://tobiasfella.de/posts/neochat-e2ee/)

But that's not all, aside from the end-to-end encryption implementation, there was also a lot of changes to NeoChat's configuration settings. James and Carl ported many settings to the new form components. James additionally created a new settings component for managing your notifications settings directly from NeoChat. Gary Wang made it possible to configure a proxy for NeoChat. Tobias improved the settings on Android (hiding the irrelevant settings).

Tobias implemented a basic developer tool that allows inspecting raw matrix events.

Carl added a confirmation dialog when signing out and Tobias added another confirmation dialog when enabling end-to-end encryption.

Tobias rewrote the account switcher to make it easier to switch between accounts.

{{< screenshots name="neochat" >}}

## Kasts

Bart added support for streaming to Kasts and episodes can now be listened to without the need to download them first. For people that don't care about downloading episodes, there is a new setting allowing you to select streaming over downloading. If this setting is activated, it will show streaming buttons on the UI instead of *download* and *play* buttons.

![Kasts with the "Prefer streaming" setting activated](kasts-streaming.png)

## Settings

Devin did some major fixes to the cellular network settings module, ensuring that the toggle state always matches the one used in the shell. He also improved behavior for when there is no SIM, as well as added more helpful messages if an APN is not configured. Some UI issues on the APN page were also fixed.

Devin also fixed accent colors being set from the wallpaper not working in the colors settings module.

## Raven

Devin fixed some issues with the new account setup. At Akademy we discussed sharing code between Kalendar and Raven. 

Łukasz fixed the edit button showing on the time page even when no entries are listed.

## Audiotube

Jonah implemented a lyrics view in the player, and made it possible to filter recent search queries. He also added real album cover images instead of monochrome icons in all song lists.

Mathis made a few UI improvements, including rounded images and new list headers.

{{< screenshots name="audiotube" >}}

Actions for each song (like *add to queue*, etc.) are now in a popup menu. This allows you to favorite songs without having to play them.

## Contributing

Want to help with the development of Plasma Mobile? Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) gives information on how and where to report issues. Also, consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!
