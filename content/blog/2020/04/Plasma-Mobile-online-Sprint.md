---
author: Plasma Mobile team
created_at: 2020-04-08 16:00:00 +0200
date: "2020-04-08T00:00:00Z"
title: 'Plasma Mobile: Join our online sprint!'
---

To foster the evolution of Plasma Mobile and bring us closer to Plasma Mobile 1.0 we are hosting an online sprint this week. We see this as a perfect opportunity to get new people involved and ask everyone interested to join us.

We will have two days of discussion about various mobile-related topics as well as a day dedicated to onboarding new people. On top of that, we are having an AMA with the core developers on [/r/kde](https://www.reddit.com/r/kde/).

The sprint is taking place on our [BigBlueButton instance](https://bigbluebutton.kde.levellingup.co.uk/b/bhu-t7z-thd).

## Discussion days

We will kick off on Thursday, April 9th, 16:00 CEST by going over the [list of tasks marked for the 1.0 milestone](https://phabricator.kde.org/project/view/247/). Later we will talk about improving our documentation, the upcoming Qt6 / KF6 transition and the release status of our components.

On the second discussion day, we will focus on our individual apps, discuss current issues and future plans. Depending on the training day this will be either Friday or Saturday.

## Training days

One day is dedicated to onboarding new people. We will have lections on how to test Plasma mobile on a desktop system, how to make use of Kirigami and how to use our apps on Android. If you are interested in joining please [tell us your preferred time](https://nuudel.digitalcourage.de/MFelhdQMabWylGiP). Depending on demand this will be either Friday or Saturday.

## AMA

On Friday, April 10th 18:00 CEST we are having an ask-me-anything session on Reddit where you can ask questions about Plasma Mobile. We will share a link to it soon.
