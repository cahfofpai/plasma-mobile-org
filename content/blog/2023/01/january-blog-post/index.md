---
title: "The Dev Log: January 2023"
subtitle: "Updates in Plasma Mobile from November - January"
SPDX-License-Identifier: CC-BY-4.0
date: 2023-01-30
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2023 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2023 Bart De Vries <bart@mogwai.be>
- SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js

spacebar:
- name: Spacebar new member display and chat details page
  url: spacebar-members.png
- name: Spacebar new scroll down button
  url: spacebar-scroll.png

kasts:
- name: Kasts new header and sidebar design
  url: kasts-desktop-new-header-design.png
- name: Kasts new header bar in collapsed mode
  url: kasts-desktop-new-header-collapsed.png
- name: Kasts new settings design showing audio backend selection
  url: kasts-new-settings.png
- name: Kasts showing chapter images
  url: kasts-desktop-chapter-images.png
- name: Kasts showing fullscreen episode chapter image
  url: kasts-desktop-fullscreen-image.png
- name: Kasts mobile interface showing clickable timestamps
  url: kasts-mobile-clickable-timestamps.png
- name: Kasts new header in maximized mode
  url: kasts-desktop-new-header-maximized.png

koko:
- name: Koko new highlight effect in the the album view
  url: koko-highlight.png
- name: Koko new settings page
  url: koko-settings.png
- name: Koko dialog for discarding non-saved edits
  url: koko-discard-editing.png

tokodon:
- name: Tokodon new timeline design
  url: https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/main.png
- name: Tokodon search feature
  url: https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/search.png
- name: Polls in Tokodon
  url: https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/poll.png
- name: Account editing
  url: https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/account-editor.png

arianna:
- name: Arianna library management
  url: arianna-library.png
- name: Arianna epub reading and searching
  url: arianna-ebook-reader.png
- name: Arianna on the PineNote
  url: arianna-pinenote.jpg

plasmatube:
- name: PlasmaTube on mobile
  url: plasmatube-mobile.png
- name: PlasmaTube homepage
  url: plasmatube-home.png
- name: PlasmaTube playing video
  url: plasmatube-video.png

alligator:
- name: Alligator Desktop
  url: alligator-desktop.png
- name: Alligator Mobile
  url: alligator-mobile.png
---

The Plasma Mobile team is happy to announce all the new developments carried out in the project from November to January!

## Housekeeping

As announced in the last blog post, we had decided to migrate the releases of Plasma Mobile applications to KDE Gear, starting with KDE Gear 23.04 (in April).

Because of this, we will now decouple the blog post format from any sort of software release schedule, and try to get one out at least on a bi-monthly basis!

The blog post you are currently reading still coincides though with the release of Plasma Mobile Gear 23.01, which is the last Mobile Gear release.

## Plasma 5.27

Plasma 5.27, the last in the Plasma 5 series, will be released on February 14th, 2023. Work will then shift to Plasma 6 from then on, with the Plasma 5.27 branch only receiving bug fixes.

### Lockscreen

A major cause of crashes with the lockscreen when multiple lockscreen requests were received in a short amount of time, was fixed (Devin Lin, Plasma 5.26.5, [Link](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/116)).

Fixed wallpaper displaying in kscreenlocker. This allowed us to remove a slow workaround. (Devin Lin, Plasma 5.27, [Link 1](https://invent.kde.org/plasma/kscreenlocker/-/merge_requests/115), [Link 2](https://invent.kde.org/plasma/plasma-mobile/-/commit/917a972e8373d6b27650d5079a6e15d032c476c0))

### Navigation Gesture

An issue was fixed where only part of the bottom edge of the screen was usable for the gesture with landscape on phones and tablets. (Devin Lin, Plasma 5.27, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/bdcbe4d6f78b606e1ecc7efae2527b0cacfe73ac))

### Other

Some issues with the mobile shell during rotation were investigated, which allowed for some issues with infinite loops to be fixed. (Devin Lin, Plasma 5.27, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/ee4ba6815a7146d9ec395d9de7fbd0c2a991d955))

The issue where translations not being picked up and created for large parts of the mobile shell was also fixed. (Devin Lin, Plasma 5.27, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/6b843da34b5e4f72ab74fd59d610d2e4a19cb735))

## Clock

The sidebar was changed to be tab bar-based. This saves a lot of horizontal space in the application. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kclock/-/commit/7ea9002fcf4b105614355e63fba64d24e04422cc))

A possible scenario where an alarm ring would be triggered when a timer is paused was fixed (bug found in Plasma shell). (Devin Lin, Plasma 5.27, [Link](https://invent.kde.org/plasma/powerdevil/-/commit/a5575edc8bb7da61a7ae0e4237f699203b367b38))

The timer add minute button was fixed. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kclock/-/commit/8eaba7b58a75093199ab7d3532154cc2c35a65c6))

## PlasmaTube (Youtube client)

The backend was ported to be libmpv-based, which greatly improves video playback and allows for seeking. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/plasmatube/-/commit/57bd6b7c019ac97a3422e1cecc6b94e7265a8c3f))

The application was redesigned, giving it the ability to have videos playing while navigating other pages. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/plasmatube/-/commit/64f5b1ddb8f396862ab31fee4ebf73bacea72926) and many more commits...)

{{< screenshots name="plasmatube" >}}

## QMLKonsole

Support was added for CLI arguments when launching the application, to start from certain folders. or run commands immediately. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/qmlkonsole/-/commit/66d78612448865c6199f8ebdf0fb74ffdf820224))

An issue where Ctrl-D would quit the entire application, rather than just a single tab was fixed. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/qmlkonsole/-/commit/e1f5b52e79c3f8bb3cb54032c0135452b800026f))

## Raven Mail

Devin is in the process of rewriting the mail sync backend to not depend on Akonadi. Work is steadily progressing, but will likely be months away from initial completion.

## Alligator (RSS Reader)

The interface was reworked to make better use of space for widescreen configurations. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/alligator/-/commit/4e70c38a395dcd77eb60775182e6cafb01a1e84d))

{{< screenshots name="alligator" >}}

## Spacebar (SMS/MMS)

Spacebar has seen several UI enhancements:

* Devin Ported the settings page to use mobile form components.
* Added a button to quickly scroll down to most recent message. The button also shows the number of new messages that have arrived since scrolling up.
* Michael improved how the members of a conversation are displayed. Spacebar now show how many more people are in conversation if all members cannot be displayed in the chats list and also on the page header of the current conversation.
* Added page to display all the members in current conversation and add new members.
* Fix contacts list scrolling not working.

Spacebar also received various performance improvements:

* Opening Spacebar is now faster as a result of the chats list query now being 5-10x faster or more in some cases.
* Smoother scrolling through messsages.
* The chats list is no longer fully reloaded every time a change happens (i.e., a message arriving).

The backend daemon also got some work done:

* Now using the modem country code for consistent formatting of phone numbers. This also prevents guessing the regional code incorrectly.
* Now verifying that received message timestamps are valid to prevent chronologically misordered messages.
* Due to some some recent changes by cetain carriers, the user agent is now faked for sending/recieving MMS.
* Now getting interface name and DNS servers directly from modem.
* Moved send message logic and MMS network logic into daemon. This prevents incomplete sending of a message if the client app gets closed or switched to a different conversation during the message sending process.
* Handle missing url id in notification messages. This should fix a MMS downloading issue that was caused by some carriers not including this.

{{< screenshots name="spacebar" >}}

## Kasts (Podcast)

Kasts has seen a lot of development since the last release.

The header bar player controls have been redesigned, partly based on the design of Elisa. The header bar is resizeable and can be fully collapsed to just a minimal toolbar. (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/d869358ff5031e4a6976d2bf5bf26278583bfc83))

The left sidebar menu in desktop mode has been redesigned. It's now using similar buttons to the ones used in the bottom navigation bar in mobile mode. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/7afc43f833658bbf409fc40f05a56b79f57b05c2))

The audio backend has been re-written from scratch. It can now handle multiple backends: libVLC, gstreamer and Qt Multimedia. Kasts will use libVLC as default backend when available, which should solve common issues: volume control is now available through the main toolbar and seeking should no longer make the audio hang. (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/eb07e67f7041407c7fb65afed63a1e8ffe254a08))

The settings menu was ported to the new form components. (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/6c8d0fa4048805cc3b75759bfb597a1b99dc3906))

Support for chapter images was added. (Tobias Fella, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/6a6962e2d3468c9ee351a86b3222e6f40637b952))

Timestamps mentioned in episode descriptions are now clickable, which will make the audio jump to that point in the episode. (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/cdec0533757240bdd92d270e2f0ea58db8ef99f9))

Images in headers are now clickable. This will open a fullscreen view of the image in question. (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/5678fc049085461ad5b6c98ad8a03ce201529710))

The subscribe button is now disabled when a podcast has just been added (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/19a6e382db6f0e4566a0c4b0d5aaada9146823fb))

Several issues with right-to-left language layouts were solved. (Bart De Vries, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kasts/-/commit/a07deae71d73e56c67e47fb3c9f01364ea741554))

{{< screenshots name="kasts" >}}

## Kalk (Calculator)

Kalk has seen lots of bug squashing and UI improvements in these three months.

Expression, calculation history and result preview font size is now reactive to window size. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/d3611c09efdf14edfb599c7e3f09177ba3e5d502))

The number pad hover color is now showing consistently even if the cursor is on the button text. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/36b8fc0d65d0f5c4bcf97271602ece59c4e67dcb))

The number pad hover is disabled on mobile to prevent a button from being incorrectly highlighted on touch screens. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/ae403e46c1e54fde7a59e8fa18b1644f272c010a))

The tail of longer calculation result is now clipped instead of the head. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/605d63b79a470723dc98e8dc5aa65de1bdad2dfe))

We fixed the bug that causes the backspace button to delete functions character by character. It now properly deletes the whole function with one click. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/5d66b240f30d63eee3869ad8c136f8fe8b1f4242))

Input and result text no longer overlap in landscape mode. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/b665310cf0b053240dd480d8143a977271afef6f))

The function drawer no longer opens by default on startup, and there is no weird function drawer auto close animation on startup. (Michael Lang, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/47ead3f3dfe514082f13e8c03024f134bd4d364a))

Switching between the history page and the calculation page no longer causes infinite new pages being added to page stack. (Zekiah A, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/47afc022e551036dc9a8078027bcf9aa04426648))

The function drawer indicator is now hidden in landscape mode (Dnt Dnt, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/a8014fc418523046299c8af6fca162ecd46d8a99))

The result is automatically cleaned if the input is empty (Šimon Rataj, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/89a79ddb4b7d6b66f0c8a0e513068c7906ae26f4))

The float point precision is kept between input string to mpfr conversion (Han Young, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/f27ffa6f697f34c2b4904a8db50d11464b3e45d8) [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/6d7498b88ab84811607ad30138cffacea38cbf2b))

The application data including icon and bug report address has been updated (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/15bb93e64b13f7502f1b78fd2a1a8d0a1267b6df))

The haptic feedback on the number pad has been reduced (Devin Lin, Mobile Gear 23.01, [Link](https://invent.kde.org/plasma-mobile/kalk/-/commit/f46f84413c0fd36390ff6b00a77c321d04f6eebd))

## Koko (Image viewer)

Carl updated Koko to use the new settings component. He also added a confirmation dialog before discarding image edits and fixed the full screen and slideshow mode not working.

{{< screenshots name="koko" >}}

## Tokodon (Mastodon client)

Since the last Plasma Mobile update in November, Tokodon received multiple big releases and now supports custom emojis, searching, hashtags handling, a conversation view, polls and account editing. For the full release announcements, please check out [22.12](https://carlschwan.eu/2022/12/18/tokodon-22.11.2-release/) and [23.01](https://carlschwan.eu/2023/01/02/tokodon-23.01.0-release/). Another big release should happen in a few days, so stay tunned!

{{< screenshots name="tokodon" >}}

## Arianna (Ebook reader)

Carl and Niccolò Venerandi started working on an ePub reader created using Kirigami that should also target the [PineNote](https://www.pine64.org/pinenote/).

There is no releases yet, but Arianna is already in a usable state. Aside from the ePub viewer, it already supports searching inside books, and has a basic library management with reading progress tracking.

{{< screenshots name="arianna" >}}

## NeoChat (Matrix client)

NeoChat will now show notifications for all accounts, and not just the active one. There is also a new "compact" mode for the room list, and the room permission settings can now be configured directly from NeoChat.

It is also now possible to search in the room history, but only on non end-to-end-encrypted chats for now.

Emojis and Reactions have been significantly improved.

And finally NeoChat's developers fixed several crashes around user invitations and various other small bugs.

## KWeather

KWeather has seen many bug fixes. Locations in the location page can now be dragged, and its UI/UX has been improved. The settings page has also seen a number of bug fixes. The plasmoid now works with the latest version of KWeatherCore. (Devin Lin, Han Young)

## AudioTube (Youtube Music client)

Mathis implemented a blurred sidebar that turns into a bottom bar on mobile devices. He also redesigned the search popup.

![Audiotube new search bar](audiotube-search.png)

AudioTube now only shows one page at a time instead of multiple stacked pages, making it visually less cluttered and fixing the issue where sometimes only half of the page was displayed

![Audiotube single page view](audiotube-info.png)

On mobile, the bottom drawer now shows the information about the selected song.

![Audiotube bottom drawer on mobile](audiotube-contextmenu.png)

Théophile Giligien made it possible to remove items from the recently played section and from your search history.

Jonah took the first steps to make AudioTube Qt6 compatible. He also fixed a bug that made it impossible to play songs

## Kirigami Addons

Carl and Mathis worked on a new search popup field component for KirigamiAddons. [API doc](https://api.kde.org/frameworks/kirigami-addons/html/classSearchPopupField.html)

<div class="ratio ratio-16x9">
  <video src="/2023/01/30/january-blog-post/kirigami-addons-search.mp4" controls autoplay=true muted></video>
</div>

Joshua added an "About KDE" section to the [AboutPage](https://api.kde.org/frameworks/kirigami-addons/html/classAboutPage.html).

![About KDE page](kirigami-addons-about.png)

## Contributing

Want to help with the development of Plasma Mobile? We are ~~desperately~~ looking for new contributors, **beginners are always welcome**!

Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Even if you do not have a compatible phone or tablet, you can also help us out with application development, which can be easily done from a desktop!

View our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), and consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!

Our [issue tracker documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) also gives information on how and where to report issues.
